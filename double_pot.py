"""
Game description:
Game starts with a pot of 2. Player's point is initially 0.  
Player decides whether to double the pot before flipping the coin.  
If the player decides to double, he will invest an additional amount 
of half the current pot, while the other half will be matched.  
If the coin toss is a head then point +1, otherwise minus 1.  
Once the point is 2, player wins, net payout is half the current pot.  
If the point reaches -2, then player loses the amount he's committed, 
which is again half the pot.
Question:
Following the optimal strategy, what's the expected value of the game?  
Focal point:  
Whether to double the pot when the point is 0.
"""
import numpy as np
import pandas as pd
    

if __name__ == '__main__':
    n = 100000 # number of simulations
    
    sum_payout_1 = 0
    sum_payout_2 = 0
    sum_count = 0

    payouts_1 = [] # strategy 1 payout each time
    payouts_2 = [] # strategy 1 payout each time
    counts = [] # number of coin tosses each time
    for i in range(n):
        point = 0
        pot_1 = 2
        pot_2 = 2
        count = 0
        while (point < 2) and (point > -2):
            if point >= 0: # double pot when 0 and 1 point
                pot_1 *= 2   

            if point >= 1: # double pot when 1 point
                pot_2 *= 2

            coin_toss = np.random.randint(0, 2)
            count += 1
            if coin_toss: 
                point += 1
            else:
                point -= 1
        payout_1 = 0.25 * pot_1 * point # net payout is half the current pot
        payout_2 = 0.25 * pot_2 * point
        sum_payout_1 += payout_1
        sum_payout_2 += payout_2
        payouts_1.append(payout_1)
        payouts_2.append(payout_2)
        sum_count += count
        counts.append(count)

    print(f"Simulated {n} times.")
    print(f"Strategy 1 average payout: {sum_payout_1 / n}.")
    print(f"Strategy 2 average payout: {sum_payout_2 / n}.")
    print(f"Average coin tosses: {sum_count / n}.")
        
    df = pd.DataFrame(
        {
            'Strategy_1': payouts_1,
            'Strategy_2': payouts_2,
            '#Coin_Toss': counts,
            }
        )
    pct = [r / 100 for r in range(0, 105, 5)]
    print(df.describe(pct)) # describe the distribution

