# Coin Game Simulation

This project is for simulation of all kinds of coin-tossing games, 
or any games that can be simulated for an answer.

The first game included is the coin game where player is to decide 
whether to double the pot before tossing the coin at each round. 
Check out the python program `double_pot.py` for more info.  